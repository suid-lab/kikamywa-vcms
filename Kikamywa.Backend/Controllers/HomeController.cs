﻿// <copyright file="HomeController.cs" company="SUID">
// This file is part of Suid.OpenTools.
//
// Suid.OpenTools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Suid.OpenTools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Suid.OpenTools.  If not, see &lt;https://www.gnu.org/licenses/&gt;.
// </copyright>

namespace Kikamywa.Backend.Controllers
{
	using System.Diagnostics;

	using Kikamywa.Backend.Models;

	using Microsoft.AspNetCore.Mvc;

	/// <summary>
	/// Home Controller.
	/// </summary>
	public class HomeController : Controller
	{
		/// <summary>
		/// Injected logger.
		/// </summary>
		private readonly ILogger<HomeController> logger;

		/// <summary>
		/// Initializes a new instance of the <see cref="HomeController"/> class.
		/// </summary>
		/// <param name="logger">A logger.</param>
		public HomeController(ILogger<HomeController> logger)
		{
			this.logger = logger;
		}

		/// <summary>
		/// Gets the index page.
		/// </summary>
		/// <returns>The index page.</returns>
		public IActionResult Index()
		{
			return this.View();
		}

		/// <summary>
		/// Gets the privacy page.
		/// </summary>
		/// <returns>The pricacy page.</returns>
		public IActionResult Privacy()
		{
			return this.View();
		}

		/// <summary>
		/// Gets the error page.
		/// </summary>
		/// <returns>The error page.</returns>
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
		}
	}
}